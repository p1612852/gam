#-------------------------------------------------
#
# Project created by QtCreator 2018-08-28T10:55:17
#
#-------------------------------------------------

QT       += core gui opengl
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Mesh_Computational_Geometry
TEMPLATE = app

SOURCES += src/main.cpp\
    src/misc/mat.cpp \
    src/ui/mainwindow.cpp \
    src/ui/gldisplaywidget.cpp \
    src/geometricworld/geometricworld.cpp \
    src/geometricworld/mesh.cpp \
    src/geometricworld/meshloader.cpp \
    src/geometricworld/meshiterators.cpp \
    src/geometricworld/objects.cpp

HEADERS  += src/ui/mainwindow.h \
    src/misc/misc.h \
    src/misc/mat.h \
    src/misc/random.h\
    src/ui/gldisplaywidget.h \
    src/geometricworld/geometricworld.h \
    src/geometricworld/mesh.h \
    src/geometricworld/meshloader.h \
    src/geometricworld/meshiterators.h \
    src/geometricworld/objects.h

INCLUDEPATH += src/geometricworld \
    src/ui \
    src/misc\
    src/

FORMS    += src/ui/mainwindow.ui

#---- Comment the following line on MacOS
LIBS = -lGLU

#---- Uncomment the following line on Windows
#LIBS += -lglu32
#LIBS += -lOpengl32

