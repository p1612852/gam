#include <cmath>
#include "mat.h"
#include "misc.h"

Point operator*(const Point &p, float scalar)
{
    return {p.x * scalar, p.y * scalar, p.z * scalar};
}

Point operator*(float scalar, const Point &p)
{
    return p * scalar;
}

Point operator+(const Point &a, const Point &b)
{
    return {a.x + b.x, a.y + b.y, a.z + b.z};
}

double distance_squared(const Point &a, const Point &b)
{
    return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z);
}

double distance(const Point &a, const Point &b)
{
    return std::sqrt(distance_squared(a, b));
    // return length(b - a);
}

Vector operator-(const Point &a, const Point &b)
{
    return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

double dot(const Vector &u, const Vector &v)
{
    return u.x * v.x + u.y * v.y + u.z * v.z;
}

Vector cross(const Vector &u, const Vector &v)
{
    return Vector(u.y * v.z - u.z * v.y,
                  u.z * v.x - u.x * v.z,
                  u.x * v.y - u.y * v.x);
}

double length(const Vector &v)
{
    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

double tan(const Point &p, const Point &q, const Point &r)
{
    Vector qr = r - q;
    Vector qp = p - q;
    Vector qr_qp = cross(qr, qp);
    auto a = std::copysign(1, dot(qr_qp, Vector(0, 0, 1))) * (length(qr_qp) / dot(qr, qp));
    return std::isnan(a) || std::isinf(a) ? 0 : a;
}

double cos(const Point &p, const Point &q, const Point &r)
{
    Vector qr = r - q;
    Vector qp = p - q;
    return dot(qr, qp) / (length(qr) * length(qp));
}

double sin2(const Point &p, const Point &q, const Point &r)
{
    return 1 - cos(q, r, p) * cos(r, p, q);
}

double orientation(const Point &p, const Point &q, const Point &r)
{
    assert(p.z == q.z && q.z == r.z && r.z == 0); // 2D point
    return (q.x - p.x) * (r.y - p.y) - (q.y - p.y) * (r.x - p.x);
}

int where_in_triangle(const Point &p, const Point &q, const Point &r, const Point &s)
{
    return std::copysign(1, min(orientation(s, p, q),
                                orientation(s, q, r),
                                orientation(s, r, p)));
    //return (o_min < 0) * -1 + (o_min > 0);
}

bool is_in_triangle(const Point &p, const Point &q, const Point &r, const Point &s)
{
    return min(orientation(s, p, q),
               orientation(s, q, r),
               orientation(s, r, p)) > 0;
}

inline Point phi(const Point &p)
{
    return {p.x, p.y, p.x * p.x + p.y * p.y};
}

int where_in_circle(const Point &p, const Point &q, const Point &r, const Point &s)
{
    Point p_phi = phi(p);
    return std::copysign(1, -dot(cross(phi(q) - p_phi, phi(r) - p_phi), (phi(s) - p_phi)));
}

bool is_in_circle(const Point &p, const Point &q, const Point &r, const Point &s)
{
    Point p_phi = phi(p);
    return -dot(cross(phi(q) - p_phi, phi(r) - p_phi), (phi(s) - p_phi)) > 0;
}

// https://en.wikipedia.org/wiki/Barycentric_coordinate_system#Examples_of_special_points
Point circumcenter(const Point &p, const Point &q, const Point &r)
{
#if 0
    double tanP = tan(r, p, q);
    double tanQ = tan(p, q, r);
    double tanR = tan(q, r, p);
    double alpha = tanQ + tanR;
    double beta = tanR + tanP;
    double gamma = tanP + tanQ;
#else
    double alpha = sin2(r, p, q);
    double beta = sin2(p, q, r);
    double gamma = sin2(q, r, p);
#endif
    double sum = alpha + beta + gamma;
    return p * (alpha / sum) + q * (beta / sum) + r * (gamma / sum);
}