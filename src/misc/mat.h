#ifndef MAT_H
#define MAT_H

struct Point
{
    double x, y, z;

    Point() = default;

    Point(double x_, double y_, double z_) : x(x_), y(y_), z(z_)
    {}
};

Point operator*(const Point &p, float scalar);

Point operator*(float scalar, const Point &p);

Point operator+(const Point &a, const Point &b);

/// Euclidean distance squared
double distance_squared(const Point &a, const Point &b);

/// Euclidean distance
double distance(const Point &a, const Point &b);

struct Vector
{
    double x, y, z;

    Vector(double x_, double y_, double z_) : x(x_), y(y_), z(z_)
    {}
};

Vector operator-(const Point &a, const Point &b);

/// @return The dot product between u and v
double dot(const Vector &u, const Vector &v);

/// @return The cross product between u and v
Vector cross(const Vector &u, const Vector &v);

/// @return The vector's length
double length(const Vector &v);

double tan(const Point &p, const Point &q, const Point &r);

double cos(const Point &p, const Point &q, const Point &r);

/// @return The sin(2A) of an angle A defined by (p,q,r)
double sin2(const Point &p, const Point &q, const Point &r);


/// @return over 0 if trigonometric orientation, less than 0 otherwise
double orientation(const Point &p, const Point &q, const Point &r);

/// @return 1 if point s is inside triangle, 0 if on edge and -1 if outside
int where_in_triangle(const Point &p, const Point &q, const Point &r, const Point &s);

/// @return true if point s is inside triangle, false otherwise
bool is_in_triangle(const Point &p, const Point &q, const Point &r, const Point &s);

/// @return 1 if point s is inside circumscribed triangle, 0 if on edge and -1 if outside
int where_in_circle(const Point &p, const Point &q, const Point &r, const Point &s);

/// @return true if point s is in circumscribed circle formed by p,q and r
bool is_in_circle(const Point &p, const Point &q, const Point &r, const Point &s);

/// @return The circumcenter of the triangle defined by the given 3 vertices
Point circumcenter(const Point &p, const Point &q, const Point &r);


#endif //MAT_H
