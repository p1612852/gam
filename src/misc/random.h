#ifndef TP_RANDOM_H
#define TP_RANDOM_H

#include <random>

namespace Random
{
    using rand_engine_t = std::mt19937;

    // seeding globally on engine() first call
    static rand_engine_t &engine()
    {
        static rand_engine_t _rng = rand_engine_t(std::random_device()());
        return _rng;
    }

    template<typename T>
    inline T range(T min, T max)
    {
        std::uniform_int_distribution<T> uid(min, max);
        return uid(engine());
    }
}

#endif //TP_RANDOM_H
