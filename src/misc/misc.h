#ifndef MISC_H
#define MISC_H

#include <algorithm>
#include <cassert>
#include <iostream>
#include <array>
#include <cmath>

#define CONCAT(A, B) A##B
#define CCAT(A, B) CONCAT(A,B)

#if !defined(NDEBUG) && !defined(QT_NO_DEBUG)
#define BASIC_DEBUG(prefix, str) (std::cout << prefix << str << std::endl)
#define DEBUG(str) BASIC_DEBUG("[DEBUG] ", str)
#define DEBUG_APPEND(str) BASIC_DEBUG("        ", str)
#define DEBUG_CONDITION(cond, str1, str2) cond ? DEBUG(str1) : DEBUG(str2)
#define DEBUG_CONDITION_APPEND(cond, str1, str2) cond ? DEBUG_APPEND(str1) : DEBUG_APPEND(str2)
#define DEBUG_COUNTER(I) size_t I{0}
#define DEBUG_COUNTER_INCREMENT(I) I++
#else
#define DEBUG(str) (__ASSERT_VOID_CAST (0))
#define DEBUG_APPEND(str) (__ASSERT_VOID_CAST (0))
#define DEBUG_CONDITION(cond, str1, str2) (__ASSERT_VOID_CAST (0))
#define DEBUG_CONDITION_APPEND(cond, str1, str2) (__ASSERT_VOID_CAST (0))
#define DEBUG_COUNTER(I) (__ASSERT_VOID_CAST (0))
#define DEBUG_COUNTER_INCREMENT(I) (__ASSERT_VOID_CAST (0))
#endif

template<typename T>
T clamp(T value, T min, T max)
{
    return std::min(std::max(value, min), max);
}

template<typename T>
T min(T a)
{
    return a;
}

template<typename T, typename ... Args>
T min(T a, Args... args)
{
    return std::min(a, min(args...));
}

inline std::array<unsigned char, 3> index_to_color(uint idx, unsigned char center = 200, unsigned char width = 55,
                                                   double f1 = 1.666, double f2 = 2.666, double f3 = 4.666,
                                                   double p1 = 0, double p2 = 2, double p3 = 4)
{
    unsigned char r = std::sin(f1 * idx + p1) * width + center;
    unsigned char g = std::sin(f2 * idx + p2) * width + center;
    unsigned char b = std::sin(f3 * idx + p3) * width + center;
    return {r, g, b};
}

#endif //MISC_H
