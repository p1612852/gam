#include "mainwindow.h"
#include "mesh.h"
#include "meshloader.h"
#include <QApplication>
#include <iostream>

using namespace gmw;

int main(int argc, char *argv[])
{
    if (argc > 1 && strcmp(argv[1], "test") == 0)
    {
        Mesh titi;
        MeshLoader::loadFromOFF(titi, "data/cube.off");
        Iterator_on_vertices its;
        Circulator_on_faces cf;
        for (its = titi.vertices_begin(); its != titi.vertices_past_the_end(); ++its)
        {
            Circulator_on_faces cfbegin = titi.incident_faces(*its);
            int cmpt = 1;
            for (cf = cfbegin, ++cf; cf != cfbegin; cf++)
                cmpt++;
            std::cout << "valence of the vertex " << cmpt << std::endl;
        }

        return 0;
    }

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
