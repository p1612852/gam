#include <cassert>
#include <algorithm>
#include <stdexcept>

#include "meshiterators.h"

using namespace gmw;

template<typename T>
Iterator_on_object<T> &Iterator_on_object<T>::operator++()
{
    _object_idx++;
    return *this;
}

template<typename T>
Iterator_on_object<T> Iterator_on_object<T>::operator++(int) &
{
    Iterator_on_object<T> tmp(*this);
    _object_idx++;
    return tmp;
}

template<typename T>
T &Iterator_on_object<T>::operator*() const
{
    assert(_objects != nullptr);
    assert(_object_idx < _objects->size());
    return _objects->at(_object_idx);
}

template<typename T>
T *Iterator_on_object<T>::operator->() const
{
    return &**this;
}

template<typename T>
bool Iterator_on_object<T>::operator!=(const Iterator_on_object<T> &rhs) const
{
    return _object_idx != rhs._object_idx;
}

template
class gmw::Iterator_on_object<Vertex>;

template
class gmw::Iterator_on_object<Face>;

template<typename T, typename V, typename Vv, typename Vf>
Circulator_on_objects<T, V, Vv, Vf> &Circulator_on_objects<T, V, Vv, Vf>::operator++()
{
    turn(true);
    return *this;
}

template<typename T, typename V, typename Vv, typename Vf>
Circulator_on_objects<T, V, Vv, Vf> Circulator_on_objects<T, V, Vv, Vf>::operator++(int) &
{
    Circulator_on_objects<T, V, Vv, Vf> tmp(*this);
    *this = ++(*this);
    return tmp;
}

template<typename T, typename V, typename Vv, typename Vf>
Circulator_on_objects<T, V, Vv, Vf> &Circulator_on_objects<T, V, Vv, Vf>::operator--()
{
    turn(false);
    return *this;
}

template<typename T, typename V, typename Vv, typename Vf>
Circulator_on_objects<T, V, Vv, Vf> Circulator_on_objects<T, V, Vv, Vf>::operator--(int) &
{
    Circulator_on_objects<T, V, Vv, Vf> tmp(*this);
    *this = --(*this);
    return tmp;
}

template<>
Vertex &Circulator_on_vertices::operator*() const
{
    assert(_vertices != nullptr);
    assert(_adjacent_vertex_idx < _vertices->size());
    return _vertices->at(_adjacent_vertex_idx);
}

template<>
const Vertex &Circulator_on_vertices_const::operator*() const
{
    assert(_vertices != nullptr);
    assert(_adjacent_vertex_idx < _vertices->size());
    return _vertices->at(_adjacent_vertex_idx);
}

template<>
Face &Circulator_on_faces::operator*() const
{
    assert(_faces != nullptr);
    assert(_incident_face_idx < _faces->size());
    return _faces->at(_incident_face_idx);
}

template<>
const Face &Circulator_on_faces_const::operator*() const
{
    assert(_faces != nullptr);
    assert(_incident_face_idx < _faces->size());
    return _faces->at(_incident_face_idx);
}


template<typename T, typename V, typename Vv, typename Vf>
T *Circulator_on_objects<T, V, Vv, Vf>::operator->() const
{
    return &**this;
}

template<typename T, typename V, typename Vv, typename Vf>
index_t Circulator_on_objects<T, V, Vv, Vf>::next_vertex(const Vertex &vertex, index_t face_idx) const
{
    const Face &face = _faces->at(face_idx);
    FOR_RELATIVE_INDEX(i)
    {
        if (&_vertices->at(face.vertices[i]) == &vertex)
        {
            return face.vertex_mod(i + 1);
        }
    }

    throw std::runtime_error("[ITERATOR ERROR] Ill-formed mesh");
}

template<>
bool Circulator_on_vertices::operator!=(const Circulator_on_vertices &rhs) const
{
    return _adjacent_vertex_idx != rhs._adjacent_vertex_idx;
}

template<>
bool Circulator_on_vertices_const::operator!=(const Circulator_on_vertices_const &rhs) const
{
    return _adjacent_vertex_idx != rhs._adjacent_vertex_idx;
}

template<>
bool Circulator_on_faces::operator!=(const Circulator_on_faces &rhs) const
{
    return _incident_face_idx != rhs._incident_face_idx;
}

template<>
bool Circulator_on_faces_const::operator!=(const Circulator_on_faces_const &rhs) const
{
    return _incident_face_idx != rhs._incident_face_idx;
}

template<typename T, typename V, typename Vv, typename Vf>
bool Circulator_on_objects<T, V, Vv, Vf>::operator==(const Circulator_on_objects<T, V, Vv, Vf> &rhs) const
{
    return !(*this != rhs);
}

template<>
index_t Circulator_on_vertices::index() const
{
    return _adjacent_vertex_idx;
}

template<>
index_t Circulator_on_vertices_const::index() const
{
    return _adjacent_vertex_idx;
}

template<>
index_t Circulator_on_faces::index() const
{
    return _incident_face_idx;
}

template<>
index_t Circulator_on_faces_const::index() const
{
    return _incident_face_idx;
}

template<typename T, typename V, typename Vv, typename Vf>
void Circulator_on_objects<T, V, Vv, Vf>::turn(bool clockwise)
{
    assert(_vertices != nullptr);
    assert(_faces != nullptr);
    assert(_adjacent_vertex_idx < _vertices->size());
    assert(_incident_face_idx < _faces->size());

    const Face &face = _faces->at(_incident_face_idx);
    FOR_RELATIVE_INDEX(i)
    {
        if (&_vertices->at(face.vertices[i]) == _vertex)
        {
            uint local_idx = (i + 1 + !clockwise) % Face::total_vertices();
            _incident_face_idx = face.adjacent_face(local_idx);
            _adjacent_vertex_idx = next_vertex(*_vertex, _incident_face_idx);
            break;
        }
    }
}

template
class gmw::Circulator_on_objects<CIRCULATOR_TEMPLATE_PARAM(Vertex,) >;

template
class gmw::Circulator_on_objects<CIRCULATOR_TEMPLATE_PARAM(Vertex, const) >;

template
class gmw::Circulator_on_objects<CIRCULATOR_TEMPLATE_PARAM(Face,) >;

template
class gmw::Circulator_on_objects<CIRCULATOR_TEMPLATE_PARAM(Face, const) >;