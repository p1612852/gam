#include <iostream>
#include "misc.h"
#include "geometricworld.h"
#include "meshloader.h"

using namespace gmw;

#define TRY_LOAD(MESH_IDX, FILENAME) \
try {                            \
MeshLoader::loadFromOFF(_mesh[MESH_IDX++], FILENAME);\
} catch(const std::exception &e) \
{                                \
   std::cerr << "Cannot load `" << FILENAME << "`: " << e.what(); \
   MESH_IDX--;                              \
}


GeometricWorld::GeometricWorld() : _current_mesh_idx(-1),
                                   _hardcoded_mesh_names({"plane", "tetrahedron", "square pyramid", "bunny", "queen"})
{
    _mesh.resize(_hardcoded_mesh_names.size());
    ushort idx = 0;
    MeshLoader::loadRandomPlane(_mesh[idx++]);
    MeshLoader::loadTetrahedron(_mesh[idx++]);
    MeshLoader::loadSquarePyramid(_mesh[idx++]);
    TRY_LOAD(idx, "./data/bunny.xy")
    TRY_LOAD(idx, "./data/queen.off")
}

inline void draw2DVoronoi(const Mesh &mesh, bool is_wireframe, float scale)
{
    const GLenum mesh_type = is_wireframe ? GL_LINE_LOOP : GL_POLYGON;
    auto &vertices = mesh.vertices();
    for (index_t i = 0; i < vertices.size(); ++i)
    {
        const Vertex &v = vertices[i];
        glBegin(mesh_type);
        auto color = index_to_color(i);
        glColor3ub(color[0], color[1], color[2]);
        Circulator_on_faces_const cfbegin = mesh.incident_faces(v);
        FOR_CIRCULATOR(cf, cfbegin)
        {
            if (mesh.is_outside_mesh(*cf))
            {
                continue;
            }
            Point p = mesh.circumcenter(cf.index()) * scale;
            glVertex3d(p.x, p.y, p.z);
        }
        glEnd();
    }
}

inline void draw2D3D(const Mesh &mesh, bool is_wireframe, float scale)
{
    const GLenum mesh_type = is_wireframe ? GL_LINE_LOOP : GL_TRIANGLES;
    auto &faces = mesh.faces();
    for (size_t face_idx = 0; face_idx < faces.size(); ++face_idx)
    {
        glBegin(mesh_type);
        auto color = index_to_color(face_idx);
        FOR_RELATIVE_INDEX(i)
        {
            Point p = mesh.vertex(faces[face_idx].vertex(i)).position * scale;
            glColor3ub(color[0], color[1], color[2]);
            glVertex3d(p.x, p.y, p.z);
        }
        glEnd();
    }
}

inline void draw1D(const Mesh &mesh, float scale)
{
    const GLenum mesh_type = GL_LINE_LOOP;
    glBegin(mesh_type);
    for (const Vertex &v : mesh.vertices())
    {
        auto color = index_to_color(v.face);
        Point p = v.position * scale;
        glColor3ub(color[0], color[1], color[2]);
        glVertex3d(p.x, p.y, p.z);
    }
    glEnd();
}

//Example with a bBox
void GeometricWorld::draw(bool is_wireframe, bool display_axes, float scale, bool voronoi) const
{
    const Mesh &mesh = _mesh[_current_mesh_idx];

    switch (mesh.type())
    {
        case Mesh::Type::MESH1D:
            draw1D(mesh, scale);
            break;
        case Mesh::Type::MESH2D:
            if (voronoi)
                draw2DVoronoi(mesh, is_wireframe, scale);
            else
                draw2D3D(mesh, is_wireframe, scale);
            break;
        case Mesh::Type::MESH3D:
            draw2D3D(mesh, is_wireframe, scale);
            break;
        default:
            throw std::logic_error("Wrong mesh type! How is that possible...");
    }

    if (display_axes)
    {
        drawAxes();
    }
}

void GeometricWorld::drawAxes() const
{
    for (uint i = 0; i < 3; ++i)
    {
        float r = i == 0, g = i == 1, b = i == 2;
        glColor3d(r, g, b);
        glBegin(GL_LINE_STRIP);
        glVertex3f(0.f, 0.f, 0.f);
        glVertex3f(r, g, b);
        glEnd();
    }
}

void GeometricWorld::loadMesh(size_t mesh_idx, const std::string &mesh_filename)
{
    if (mesh_idx == _current_mesh_idx)
        return;

    _current_mesh_idx = mesh_idx;
    if (_current_mesh_idx >= _mesh.size())
    {
        _mesh.emplace_back();
        try
        {
            MeshLoader::loadFromOFF(_mesh.back(), mesh_filename.c_str());
        }
        catch (const std::exception &e)
        {
            std::cerr << "[ERROR] Failed to load OFF file " << mesh_filename << std::endl;
            std::cerr << "        Reason: " << e.what() << std::endl;
            _current_mesh_idx--;
        }
    }
}

const std::vector<std::string> &GeometricWorld::getMeshNames() const
{
    return _hardcoded_mesh_names;
}

Mesh &GeometricWorld::getMesh()
{
    return _mesh[_current_mesh_idx];
}

const Mesh &GeometricWorld::getMesh() const
{
    return _mesh[_current_mesh_idx];
}

void GeometricWorld::generatePlane(uint width, uint height, uint slices, bool random)
{
    MeshLoader::loadRandomPlane(_mesh[0], width, height, slices, random);
}
