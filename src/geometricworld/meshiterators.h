#ifndef MESHITERATORS_H
#define MESHITERATORS_H

#include <vector>

#include "objects.h"

namespace gmw
{
    class Mesh;

    template<typename T>
    class Iterator_on_object
    {
    public:
        Iterator_on_object() : _objects(nullptr), _object_idx(0)
        {}

        Iterator_on_object(std::vector<T> *objects, index_t object_idx) : _objects(objects), _object_idx(object_idx)
        {}

        Iterator_on_object &operator++();

        Iterator_on_object operator++(int) &;

        T &operator*() const;

        T *operator->() const;

        bool operator!=(const Iterator_on_object<T> &rhs) const;

    private:
        std::vector<T> *_objects;
        index_t _object_idx;
    };

    using Iterator_on_faces = Iterator_on_object<Face>;
    using Iterator_on_vertices = Iterator_on_object<Vertex>;

    template<typename T, typename V, typename Vv, typename Vf>
    class Circulator_on_objects
    {
    public:
        Circulator_on_objects() : _vertices(nullptr), _faces(nullptr), _vertex(nullptr), _incident_face_idx(0),
                                  _adjacent_vertex_idx(0)
        {}

        Circulator_on_objects(Vv *vertices, Vf *faces, V *vertex)
                : _vertices(vertices), _faces(faces), _vertex(vertex), _incident_face_idx(_vertex->face),
                  _adjacent_vertex_idx(next_vertex(*_vertex, _incident_face_idx))
        {}

        Circulator_on_objects(Vv *vertices, Vf *faces, V *vertex,
                              index_t starting_face)
                : _vertices(vertices), _faces(faces), _vertex(vertex), _incident_face_idx(starting_face),
                  _adjacent_vertex_idx(next_vertex(*_vertex, _incident_face_idx))
        {}

        Circulator_on_objects &operator++();

        Circulator_on_objects operator++(int) &;

        Circulator_on_objects &operator--();

        Circulator_on_objects operator--(int) &;

        T &operator*() const;

        T *operator->() const;

        bool operator!=(const Circulator_on_objects<T, V, Vv, Vf> &rhs) const;

        bool operator==(const Circulator_on_objects<T, V, Vv, Vf> &rhs) const;

        index_t index() const;

    private:
        Vv *_vertices;
        Vf *_faces;
        V *_vertex;
        index_t _incident_face_idx;
        index_t _adjacent_vertex_idx;

        index_t next_vertex(const Vertex &vertex, index_t face_idx) const;

        void turn(bool clockwise);
    };

#define CIRCULATOR_TEMPLATE_PARAM(VAR, CONST) CONST VAR, CONST Vertex, CONST std::vector<Vertex>, CONST std::vector<Face>
    using Circulator_on_vertices = Circulator_on_objects<CIRCULATOR_TEMPLATE_PARAM(Vertex,) >;
    using Circulator_on_vertices_const = Circulator_on_objects<CIRCULATOR_TEMPLATE_PARAM(Vertex, const) >;
    using Circulator_on_faces = Circulator_on_objects<CIRCULATOR_TEMPLATE_PARAM(Face,) >;
    using Circulator_on_faces_const = Circulator_on_objects<CIRCULATOR_TEMPLATE_PARAM(Face, const) >;

#define FOR_CIRCULATOR(CF, CFBEGIN) bool CCAT(stop,__LINE__) = false; for (auto CF = CFBEGIN; !CCAT(stop,__LINE__); CF++, CCAT(stop,__LINE__) = CF == CFBEGIN)
}

#endif //MESHITERATORS_H
