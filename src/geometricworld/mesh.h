#ifndef MESH_H
#define MESH_H

#include <QGLWidget>
#include <vector>
#include <set>
#include <queue>

#include "objects.h"
#include "meshiterators.h"

namespace gmw
{
    class Mesh
    {
    public:

        enum class Type
        {
            MESH3D, MESH2D, MESH1D
        };

        /// Resets the mesh
        void clear();

        Type type() const;

        bool is2D() const;

        Vertex &vertex(index_t idx);

        const Vertex &vertex(index_t idx) const;

        Face &face(index_t idx);

        const Face &face(index_t idx) const;

        const Point &circumcenter(index_t face_idx) const;

        const std::vector<Vertex> &vertices() const;

        const std::vector<Face> &faces() const;

        Iterator_on_faces faces_begin();

        Iterator_on_faces faces_past_the_end();

        Iterator_on_vertices vertices_begin();

        Iterator_on_vertices vertices_past_the_end();

        Circulator_on_faces incident_faces(Vertex &v);

        Circulator_on_faces_const incident_faces(const Vertex &v) const;

        Circulator_on_faces incident_faces(Vertex &v, index_t starting_face);

        Circulator_on_faces_const incident_faces(const Vertex &v, index_t starting_face) const;

        Circulator_on_vertices adjacent_vertices(Vertex &v);

        Circulator_on_vertices_const adjacent_vertices(const Vertex &v) const;

        Circulator_on_vertices adjacent_vertices(Vertex &v, index_t starting_face);

        Circulator_on_vertices_const adjacent_vertices(const Vertex &v, index_t starting_face) const;

        //===========================================================//
        //==================== 2D MESH FUNCTIONS ====================//
        //====================     DELAUNAY      ====================//
        //===========================================================//

        /**
         * Splits a triangle into 3 new triangles by adding the given point as their new vertex
         * @return The inserted vertex id
         */
        index_t split_triangle(index_t face_idx, const Point &p);

        /**
         * Flips an edge between two given faces.
         * Applying this function 4 times on the same faces amounts to doing nothing.
         */
        void flip_edge(index_t f1, index_t f2);

        /**
         * Finds the face the given point falls inside the 2D Mesh.
         * If the point is outside the convex hull, returns a
         * facing 'infinite' triangle index (i.e. face linked to inf. vertex).
         */
        index_t find_face(const Point &point) const;

        /**
         * Inserts a point in a 2D mesh and applies delaunay iteratively
         * by default.
         * @return The inserted vertex's index
         */
        index_t insert(const Point &point, bool apply_lawson = true);

        /**
         * Applies the lawson algorithm. Starts iteratively
         * from the given parameter if not null.
         * Otherwise, applies the algorithm to all faces.
         */
        void lawson(const index_t *vertex_idx = nullptr);

        /**
         * Reconstructs the 2D mesh (1D border)
         * by applying the crust algorithm.
         */
        void crust();

        /// Calculates faces circumcenter
        void calculate_circumcenters();

        //===========================================================//
        //===================  3D MESH FUNCTIONS  ===================//
        //=================== MESH SIMPLIFICATION ===================//
        //===========================================================//

        void contract_edge(index_t v1, index_t v2);

        /// @return all edges in a mesh without duplicates
        std::set<Edge> generate_edges() const;

        /**
         * Simplifies a mesh by contracting edges, ranking
         * them by their length.
         * @param proportion Proportion of edges to simplify (between 0 and 1).
         */
        void simplify(double proportion);

        /// @return true if a face is part of a 2D mesh (inside convex hull)
        bool is_inside_mesh(const Face &face) const;

        bool is_inside_mesh(index_t face_idx) const;

        /// @return true if a face is outside the convex hull
        bool is_outside_mesh(const Face &face) const;

        bool is_outside_mesh(index_t face_idx) const;

    private:
        Type _type = Type::MESH3D;
        std::vector<Vertex> _vertices;
        std::vector<Face> _faces;
        std::vector<Point> _circumcenters;

        friend class MeshLoader;

        /// @return A face's index inside the convex hull, chosen randomly
        index_t random_face_on_mesh() const;

        bool is_infinite_vertex(const Vertex &vertex) const;

        /**
         * Returns the relative vertex's index that has
         * the given face index as opposite.
         * @param face_idx The face opposing the vertex.
         * @return A relative vertex index.
         * @throw std::logic_error if does not exist
         */
        relative_index_t opposite_vertex_relative_idx(index_t face_idx, index_t adjacent_face_idx) const;

        /**
         * Returns the vertex's index that has
         * the given face index as opposite.
         * @param face_idx The face opposing the vertex.
         * @return A vertex index.
         * @throw std::logic_error if does not exist
         */
        index_t opposite_vertex_absolute_idx(index_t face_idx, index_t adjacent_face_idx) const;

        // wrapper
        bool is_in_triangle(const Face &face, const Point &point) const;

        // wrapper
        bool is_in_circle(const Face &face, const Point &point) const;

        /// Appends to the given queue all adjacent faces that are not delaunay
        void add_not_delaunay_triangles(std::queue<Edge> &not_delaunay, index_t face_idx) const;

        /**
         * @return The infinite vertex's index by circulating around the given vertex pivot
         * @throw std::logic_error if not found
         */
        index_t find_infinite_vertex(index_t vertex_pivot) const;

        /**
         * @return The face around the vien inserted vertex that is part of the convex hull
         * @throw std::logic_error if not found
         */
        index_t find_inserted_face(index_t inserted_vertex) const;

        /**
         * @return A const circulator on vertices around the given pivot starting at the given vertex
         * @throw std::logic_error if not found
         */
        Circulator_on_vertices_const find_vertex_around(index_t vertex_pivot, index_t vertex_to_find) const;

        template<typename F>
        index_t next_adjacent_vertex_with_condition(index_t vertex_idx, F f) const
        {
            Circulator_on_vertices_const cvbegin = adjacent_vertices(vertex(vertex_idx));
            bool stop = false;
            for (auto cv = cvbegin; !stop; cv++, stop = cv == cvbegin)
            {
                if (f(cv.index()))
                {
                    return cv.index();
                }
            }
            return vertex_idx;
        }
    };

}
#endif // MESH_H
