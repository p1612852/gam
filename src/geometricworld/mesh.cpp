#include <sstream>
#include <stack>
#include "misc.h"
#include "random.h"
#include "mesh.h"

using namespace gmw;

void Mesh::clear()
{
    _vertices.clear();
    _faces.clear();
}

Mesh::Type Mesh::type() const
{
    return _type;
}

bool Mesh::is2D() const
{
    return _type == Type::MESH2D;
}

Vertex &Mesh::vertex(index_t idx)
{
    return _vertices[idx];
}

const Vertex &Mesh::vertex(index_t idx) const
{
    return _vertices[idx];
}

Face &Mesh::face(index_t idx)
{
    return _faces[idx];
}

const Face &Mesh::face(index_t idx) const
{
    return _faces[idx];
}

const Point &Mesh::circumcenter(index_t face_idx) const
{
    return _circumcenters[face_idx];
}

const std::vector<Vertex> &Mesh::vertices() const
{
    return _vertices;
}

const std::vector<Face> &Mesh::faces() const
{
    return _faces;
}

Iterator_on_faces Mesh::faces_begin()
{
    return Iterator_on_faces(&_faces, 0);
}

Iterator_on_faces Mesh::faces_past_the_end()
{
    return Iterator_on_faces(&_faces, _faces.size());
}

Iterator_on_vertices Mesh::vertices_begin()
{
    return Iterator_on_vertices(&_vertices, 0);
}

Iterator_on_vertices Mesh::vertices_past_the_end()
{
    return Iterator_on_vertices(&_vertices, _vertices.size());
}

Circulator_on_faces Mesh::incident_faces(Vertex &v)
{
    return Circulator_on_faces(&_vertices, &_faces, &v);
}

Circulator_on_faces_const Mesh::incident_faces(const Vertex &v) const
{
    return Circulator_on_faces_const(&_vertices, &_faces, &v);
}

Circulator_on_faces Mesh::incident_faces(Vertex &v, index_t starting_face)
{
    return Circulator_on_faces(&_vertices, &_faces, &v, starting_face);
}

Circulator_on_faces_const Mesh::incident_faces(const Vertex &v, index_t starting_face) const
{
    return Circulator_on_faces_const(&_vertices, &_faces, &v, starting_face);
}

Circulator_on_vertices Mesh::adjacent_vertices(Vertex &v)
{
    return Circulator_on_vertices(&_vertices, &_faces, &v);
}

Circulator_on_vertices_const Mesh::adjacent_vertices(const Vertex &v) const
{
    return Circulator_on_vertices_const(&_vertices, &_faces, &v);
}

Circulator_on_vertices Mesh::adjacent_vertices(Vertex &v, index_t starting_face)
{
    return Circulator_on_vertices(&_vertices, &_faces, &v, starting_face);
}

Circulator_on_vertices_const Mesh::adjacent_vertices(const Vertex &v, index_t starting_face) const
{
    return Circulator_on_vertices_const(&_vertices, &_faces, &v, starting_face);
}


//===========================================================//
//==================== 2D MESH FUNCTIONS ====================//
//====================     DELAUNAY      ====================//
//===========================================================//

index_t Mesh::split_triangle(index_t face_idx, const Point &p)
{
    // inserting new vertex
    _vertices.emplace_back(p, face_idx);
    index_t inserted_vertex_idx = _vertices.size() - 1;

    // generating two new faces -- doing it before because
    // emplace_back may invalidate references to any face
    for (uchar i = 0; i < 2; ++i)
    {
        _faces.emplace_back();
    }

    Face &split_face = _faces[face_idx];
    // updating 2nd adjacent face of split face
    for (relative_index_t i = 0; i < 2; ++i)
    {
        index_t new_face_idx = _faces.size() - (2 - i);

        index_t adj_face_idx = split_face.adjacent_face(1 + i);
        split_face.set_adjacent_face(1 + i, new_face_idx);
        relative_index_t v_idx_local = opposite_vertex_relative_idx(face_idx, adj_face_idx);
        // updating adjacent face's adjacent face (mouthful)
        face(adj_face_idx).set_adjacent_face(v_idx_local, new_face_idx);
        // updating first new face
        face(new_face_idx).vertices = {split_face.vertex(i), inserted_vertex_idx, split_face.vertex_mod(2 + i)};
        if (i == 0)
        {
            face(new_face_idx).adjacent_faces = {face_idx, adj_face_idx, new_face_idx + 1};
        }
        else
        {
            face(new_face_idx).adjacent_faces = {new_face_idx - 1, adj_face_idx, face_idx};
        }

    }

    vertex(split_face.vertices[0]).face = _faces.size() - 1;
    // setting split face new vertex
    split_face.vertices[0] = inserted_vertex_idx;
    return inserted_vertex_idx;
}

void Mesh::flip_edge(index_t f1, index_t f2)
{
    //TODO: check if edge is flippable? (would require two orientation tests)
    // no error so far but probably because I always test for orientation > 0 and not >= 0

    // ugly but works (I don't want to dive in the code anymore)
    std::array<index_t, 2> faces_idx = {f1, f2};
    std::array<relative_index_t, 2> opp_v_idx = {opposite_vertex_relative_idx(f2, f1),
                                                 opposite_vertex_relative_idx(f1, f2)};
    std::array<index_t, 2> tmp = {face(faces_idx[1]).adjacent_face_mod(opp_v_idx[1] + 2),
                                  face(faces_idx[0]).adjacent_face_mod(opp_v_idx[0] + 2)};
    for (uchar i = 0, j = 1; i < 2; ++i, j = (i + 1) % 2)
    {
        Face &face_main = face(faces_idx[i]);
        Face &face_other = face(faces_idx[j]);
        vertex(face_main.vertex_mod(opp_v_idx[i] + 2)).face = faces_idx[i];
        face_main.set_vertex_mod(opp_v_idx[i] + 1, face_other.vertex(opp_v_idx[j]));
        face_main.set_adjacent_face_mod(opp_v_idx[i] + 2, face_main.adjacent_face(opp_v_idx[i]));
        face_main.set_adjacent_face(opp_v_idx[i], tmp[i]);

        index_t new_adj_face = face_main.adjacent_face(opp_v_idx[i]);
        relative_index_t v_idx = opposite_vertex_relative_idx(faces_idx[j], new_adj_face);
        face(new_adj_face).adjacent_faces[v_idx] = faces_idx[i];
    }
}

index_t Mesh::find_face(const Point &point) const
{
    index_t face_idx = random_face_on_mesh();
    DEBUG_COUNTER(counter);
    // walking algorithm
    while (true)
    {
        const Face &face = _faces[face_idx];
        if (is_outside_mesh(face) || is_in_triangle(face, point))
        {
            DEBUG_APPEND("Walked through " << counter << " faces");
            return face_idx;
        }

        // checking which half-space defined by face_idx's edge contains the given point
        FOR_RELATIVE_INDEX(i)
        {
            const Vertex &v1 = vertex(face.vertex_mod(i));
            const Vertex &v2 = vertex(face.vertex_mod(i + 1));
            if (orientation(v1.position, point, v2.position) > 0)
            {
                face_idx = face.adjacent_face_mod(i + 2);
                break;
            }
        }
        DEBUG_COUNTER_INCREMENT(counter);
    }
}

index_t Mesh::insert(const Point &point, bool apply_lawson)
{
    assert(point.z == 0);
    DEBUG("Inserting point (" << point.x << "," << point.y << "," << point.z << ")");
    index_t face_idx = find_face(point);
    bool is_outside = is_outside_mesh(face_idx);
    DEBUG_APPEND("Splitting closest face of id " << face_idx);
    const index_t inserted_vertex = split_triangle(face_idx, point);

    if (is_outside) // infinite edges flip
    {
        index_t infinite_vertex = find_infinite_vertex(inserted_vertex);
        index_t inserted_face = find_inserted_face(inserted_vertex);

        const Circulator_on_vertices_const TMP = find_vertex_around(infinite_vertex, inserted_vertex);

        const Vertex &inf_vertex = vertex(infinite_vertex);
        relative_index_t relative_inserted_vertex = face(inserted_face).relative(inserted_vertex);
        std::array<Circulator_on_vertices_const, 2> cvs;
        // left side
        index_t left_infinite_face = face(inserted_face).adjacent_face_mod(relative_inserted_vertex + 1);
        bool stop = false;
        while (!stop)
        {
            Circulator_on_vertices_const cv = adjacent_vertices(inf_vertex, left_infinite_face);
            Circulator_on_faces_const cf = incident_faces(inf_vertex, left_infinite_face);
            cvs[0] = ++cv;
            cvs[1] = ++cv;
            if (orientation(TMP->position, cvs[0]->position, cvs[1]->position) > 0)
            {
                index_t f1 = cf.index();
                index_t f2 = (++cf).index();
                left_infinite_face = f1;
                flip_edge(f1, f2);
                continue;
            }
            stop = true;
        }

        // right side
        index_t right_infinite_face = face(inserted_face).adjacent_face_mod(relative_inserted_vertex + 2);
        stop = false;
        while (!stop)
        {
            Circulator_on_vertices_const cv = adjacent_vertices(inf_vertex, right_infinite_face);
            Circulator_on_faces_const cf = incident_faces(inf_vertex, right_infinite_face);
            cvs[0] = cv;
            cvs[1] = --cv;
            if (orientation(TMP->position, cvs[1]->position, cvs[0]->position) > 0)
            {
                index_t f1 = cf.index();
                index_t f2 = (--cf).index();
                right_infinite_face = f2;
                flip_edge(f1, f2);
                continue;
            }
            stop = true;
        }
    }

    if (apply_lawson)
        lawson(&inserted_vertex);

    return inserted_vertex;
}

void Mesh::lawson(const index_t *vertex_idx)
{
    assert(is2D());
    DEBUG_CONDITION(vertex_idx == nullptr, "Applying lawson's algorithm globally",
                    "Applying lawson's algorithm incrementally");
    std::queue<Edge> not_delaunay;
    if (vertex_idx == nullptr) // global delaunay
    {
        for (index_t idx = 0; idx < _faces.size(); ++idx)
        {
            if (is_inside_mesh(idx))
            {
                add_not_delaunay_triangles(not_delaunay, idx);
            }
        }
    }
    else // starting from specific vertex
    {
        const Vertex &inserted_vertex = vertex(*vertex_idx);
        Circulator_on_faces_const cfbegin = incident_faces(inserted_vertex);
        FOR_CIRCULATOR(cf, cfbegin)
        {
            relative_index_t v = cf->relative(*vertex_idx);
            if (is_inside_mesh(*cf) && is_inside_mesh(cf->adjacent_face(v)))
            {
                not_delaunay.push({cf.index(), cf->adjacent_face(v)});
            }
        }
    }
    DEBUG_APPEND(not_delaunay.size() << " edges to check and potentially flip");

    DEBUG_COUNTER(flipped_edges_count);
    while (!not_delaunay.empty())
    {
        const Edge &faces = not_delaunay.front();
        if (!face(faces.first).is_adjacent_face(faces.second))
        {
            not_delaunay.pop();
            continue;
        }

        relative_index_t v = opposite_vertex_relative_idx(faces.first, faces.second);
        const Face &face_first = face(faces.first);
        const Face &face_second = face(faces.second);
        if (is_in_circle(face_first, vertex(face_second.vertex(v)).position))
        {
            index_t face_to_test1 = face_second.adjacent_face_mod(v + 2);
            index_t face_to_test2 = face_second.adjacent_face_mod(v + 1);

            flip_edge(faces.first, faces.second);
            DEBUG_COUNTER_INCREMENT(flipped_edges_count);

            if (is_inside_mesh(face_to_test1))
                not_delaunay.push({faces.first, face_to_test1});
            if (is_inside_mesh(face_to_test2))
                not_delaunay.push({faces.second, face_to_test2});
        }

        not_delaunay.pop();
    }

    DEBUG_APPEND(flipped_edges_count << " edges flipped");
}

void Mesh::crust()
{
    // precondition calculate_circumcenters();
    //TODO: if changing circumcenter calculation
    // (instead of recalculating everything)
    // then copy the circumcenters otherwise
    // they will change as vertices are inserted
    // std::set<index_t> inserted_vertices;

    const index_t total_sampled_input_vertices = _vertices.size();
    const size_t total_faces = _faces.size();
    for (index_t face_idx = 0; face_idx < total_faces; ++face_idx)
    {
        if (is_inside_mesh(face_idx))
        {
            insert(circumcenter(face_idx));
        }
    }

    // does not work, and it's a mess. Maybe my lawson algorithm isn't correct?
    // or there are edge cases I do not take account for
    std::vector<Vertex> new_vertices;
    std::set<index_t> already_seen_vertices;
    std::stack<index_t> vertices_to_check;
    vertices_to_check.push(0);
    while (new_vertices.size() != total_sampled_input_vertices)
    {
        const Vertex &v = vertex(vertices_to_check.top());
        vertices_to_check.pop();
        const Circulator_on_vertices_const cvbegin = adjacent_vertices(v);
        FOR_CIRCULATOR(cv, cvbegin)
        {
            index_t adjacent_vertex = cv.index();
            if (adjacent_vertex < total_sampled_input_vertices
                && already_seen_vertices.count(adjacent_vertex) == 0
                && !is_infinite_vertex(vertex(adjacent_vertex)))
            {
                already_seen_vertices.emplace(adjacent_vertex);
                vertices_to_check.push(adjacent_vertex);
            }
        }
        if (!is_infinite_vertex(v))
        {
            new_vertices.emplace_back(v);
        }
    }

    _vertices = std::move(new_vertices);
    _faces.clear();
    _type = Mesh::Type::MESH1D;
}

void Mesh::calculate_circumcenters()
{
    _circumcenters.clear();
    _circumcenters.reserve(_faces.size());
    for (const Face &face : _faces)
    {
        Point point = ::circumcenter(vertex(face.vertex(0)).position,
                                     vertex(face.vertex(1)).position,
                                     vertex(face.vertex(2)).position);
        _circumcenters.emplace_back(std::move(point));
    }
}

//===========================================================//
//===================  3D MESH FUNCTIONS  ===================//
//=================== MESH SIMPLIFICATION ===================//
//===========================================================//

void Mesh::contract_edge(__attribute__((unused))index_t v1, __attribute__((unused))index_t v2)
{
    //TODO
}

std::set<Edge> Mesh::generate_edges() const
{
    // listing edges
    std::set<Edge> edges;
    for (const Face &face : _faces)
    {
        FOR_RELATIVE_INDEX(i)
        {
            index_t v1 = face.vertex(i);
            index_t v2 = face.vertex_mod(i + 1);
            if (edges.count({v2, v1}) > 0)
                continue;
            edges.insert({v1, v2});
        }
    }

    return edges;
}

void Mesh::simplify(__attribute__((unused))double proportion)
{
    assert(proportion > 0 && proportion <= 1);

#if 0 // teaser
    using pair_t = std::pair<Edge,uint>;
    auto cmp = [](const pair_t &left, const pair_t &right) { left.second > right.second; };
    std::priority_queue<pair_t, std::deque<pair_t>, decltype(cmp)> edges_to_contract(cmp);

    std::set<Edge> edges = generate_edges();
    for (const Edge &edge : edges)
    {
        const Vertex &v1 = vertex(edge.first);
        const Vertex &v2 = vertex(edge.second);
        edges_to_contract.emplace(edge, distance_squared(v1.position, v2.position));
    }

    size_t progress = 0;
    size_t max = edges.size();
    size_t limit = proportion * max;
    while (!edges_to_contract.empty() && progress < limit)
    {
        const Edge &edge = edges_to_contract.top().first;

        contract_edge(edge.first, edge.second);
        //TODO: when contracting an edge, other edges change...
        // voir dijsktra mais en gros, plutôt faire une file avec pair<Edge,Dist> comme
        // ça on peut recalculer et vérifier que l'edge vérifie encore les conditions
        // ==> c'est déjà ce que tu as, nez de bœuf

        edges_to_contract.pop();
        progress++;
    }
#endif
}

index_t Mesh::random_face_on_mesh() const
{
    // to get rid of this, use distinct containers for in_mesh and
    // outside_mesh faces
    index_t face_idx;
    do
    {
        face_idx = Random::range(0UL, _faces.size() - 1);
    } while (is_outside_mesh(face_idx));
    return face_idx;
}

bool Mesh::is_inside_mesh(const Face &face) const
{
    for (const index_t v : face.vertices)
        if (vertex(v).position.z != 0)
            return false;

    return true;
}

bool Mesh::is_inside_mesh(index_t face_idx) const
{
    return is_inside_mesh(face(face_idx));
}

bool Mesh::is_outside_mesh(const Face &face) const
{
    return !is_inside_mesh(face);
}

bool Mesh::is_outside_mesh(index_t face_idx) const
{
    return !is_inside_mesh(face(face_idx));
}

bool Mesh::is_infinite_vertex(const Vertex &vertex) const
{
    return vertex.position.z != 0;
}

relative_index_t Mesh::opposite_vertex_relative_idx(index_t face_idx, index_t adjacent_face_idx) const
{
    FOR_RELATIVE_INDEX(i)
    {
        if (_faces[adjacent_face_idx].adjacent_face(i) == face_idx)
        {
            return i;
        }
    }
    std::ostringstream s;
    s << "This face (" << face_idx << ") is not the opposite of a vertex of the given adjacent face ("
      << adjacent_face_idx << ")";
    throw std::logic_error(s.str());
}

index_t Mesh::opposite_vertex_absolute_idx(index_t face_idx, index_t adjacent_face_idx) const
{
    return face(adjacent_face_idx).vertex(opposite_vertex_relative_idx(face_idx, adjacent_face_idx));
}

bool Mesh::is_in_triangle(const Face &face, const Point &point) const
{
    return ::is_in_triangle(vertex(face.vertex(0)).position, vertex(face.vertex(1)).position,
                            vertex(face.vertex(2)).position, point);
}

bool Mesh::is_in_circle(const Face &face, const Point &point) const
{
    return ::is_in_circle(vertex(face.vertex(0)).position, vertex(face.vertex(1)).position,
                          vertex(face.vertex(2)).position, point);
}

void Mesh::add_not_delaunay_triangles(std::queue<Edge> &not_delaunay, index_t face_idx) const
{
    FOR_RELATIVE_INDEX(i)
    {
        index_t adj_face = face(face_idx).adjacent_face(i);
        if (is_outside_mesh(adj_face))
            continue;
        index_t s = opposite_vertex_absolute_idx(face_idx, adj_face);
        if (is_in_circle(face(face_idx), vertex(s).position))
        {
            not_delaunay.push({face_idx, adj_face});
        }
    }
}

index_t Mesh::find_infinite_vertex(index_t vertex_pivot) const
{
    Circulator_on_vertices_const cvbegin = adjacent_vertices(vertex(vertex_pivot));
    FOR_CIRCULATOR(cv, cvbegin) // finding infinite vertex
        if (is_infinite_vertex(*cv))
            return cv.index();

    throw std::logic_error("No infinite vertex linked to " + std::to_string(vertex_pivot));
}

index_t Mesh::find_inserted_face(index_t inserted_vertex) const
{
    Circulator_on_faces_const cfbegin = incident_faces(vertex(inserted_vertex));
    FOR_CIRCULATOR(cf, cfbegin) // finding inserted face
        if (is_inside_mesh(*cf))
            return cf.index();

    throw std::logic_error(
            "Cannot find inserted face in convex hull around vertex " + std::to_string(inserted_vertex));
}

Circulator_on_vertices_const Mesh::find_vertex_around(index_t vertex_pivot, index_t vertex_to_find) const
{
    Circulator_on_vertices_const cvbegin = adjacent_vertices(vertex(vertex_pivot));
    FOR_CIRCULATOR(cv, cvbegin) // finding adjacent vertex == the inserted one
        if (cv.index() == vertex_to_find)
            return cv;

    std::ostringstream ss;
    ss << "Cannot find vertex " << vertex_to_find << " around " << vertex_pivot;
    throw std::logic_error(ss.str());
}

