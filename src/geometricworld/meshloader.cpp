#include <fstream>
#include <cmath>
#include <sstream>

#include "misc.h"
#include "random.h"
#include "meshloader.h"

using namespace gmw;

const Point infinite(0, 0, -5);

inline Mesh::Type getMeshType(const std::vector<Vertex> &vertices)
{
    bool is2D = true;
    bool one_vertex_3D = false;
    for (const Vertex &v : vertices)
    {
        if (v.position.z != 0 && !one_vertex_3D)
        {
            one_vertex_3D = true;
        }
        else if (v.position.z != 0 && one_vertex_3D)
        {
            is2D = false;
            break;
        }
    }
    return is2D ? Mesh::Type::MESH2D : Mesh::Type::MESH3D;
}

inline bool isFile2D(const char *filename)
{
    std::ifstream file(filename);
    std::string line, word;
    ushort word_count = 0;
    std::getline(file, line); // skip 1st line
    if (std::getline(file, line))
    {
        std::stringstream ss(line);
        while (ss >> word)
        {
            ++word_count;
        }
    }
    return word_count == 2;
}

void MeshLoader::loadFromOFF(Mesh &mesh, const char *filename)
{
    DEBUG("Loading OFF file `" << filename << "`...");
    mesh.clear();
    //v f e
    std::ifstream off_file(filename);

    index_t total_vertices, total_faces, total_edges;
    off_file >> total_vertices >> total_faces >> total_edges;

    bool is_file2D = isFile2D(filename);
    DEBUG_APPEND("Reading " << total_vertices << " vertices...");
    // reading each vertex
    std::vector<Vertex> vertices;
    vertices.reserve(total_vertices);
    if (is_file2D)
    {
        for (index_t i = 0; i < total_vertices; ++i)
        {
            Point p;
            off_file >> p.x >> p.y;
            p.z = .0;
            vertices.emplace_back(p);
        }
        mesh._type = Mesh::Type::MESH2D;
    }
    else
    {
        for (index_t i = 0; i < total_vertices; ++i)
        {
            Point p;
            off_file >> p.x >> p.y >> p.z;
            vertices.emplace_back(p);
        }

        mesh._type = getMeshType(vertices);
    }
    DEBUG_CONDITION_APPEND(mesh.is2D(), "Mesh is 2D", "Mesh is 3D");

    DEBUG_APPEND("Reading " << total_faces << " faces...");
    if (total_faces == 0)// scattered points without any link
    {
        DEBUG_APPEND("Generating faces...");
        if (!mesh.is2D())
        {
            throw std::logic_error("No face and mesh is not 2D!");
        }
        if (total_vertices < 3)
        {
            throw std::out_of_range(
                    "Not enough vertices, at least 3 required (found " + std::to_string(total_edges) + ")");
        }

        for (index_t i = 0; i < 3; ++i)
        {
            mesh._vertices.emplace_back(vertices[i].position, 0);
        }
        mesh._vertices.emplace_back(infinite, 1);
        const index_t infinite_vertex = mesh._vertices.size() - 1;
        mesh._faces.emplace_back(0, 1, 2, 1, 2, 3);
        mesh._faces.emplace_back(1, infinite_vertex, 2, 2, 0, 3);
        mesh._faces.emplace_back(2, infinite_vertex, 0, 3, 0, 1);
        mesh._faces.emplace_back(0, infinite_vertex, 1, 1, 0, 2);

        for (index_t i = infinite_vertex + 1; i < total_vertices; ++i)
        {
            mesh.insert(vertices[i].position);
        }
    }
    else// classic mesh
    {
        mesh._vertices = std::move(vertices);
        std::map<std::pair<index_t, index_t>, std::pair<index_t, index_t>> faces_and_opposite_vertex;
        // reading each face with adjacent faces
        mesh._faces.resize(total_faces);
        for (index_t face_idx = 0; face_idx < total_faces; ++face_idx)
        {
            auto &face = mesh.face(face_idx);

            // safety check
            index_t face_total_vertices;
            off_file >> face_total_vertices;
            if (face_total_vertices != Face::total_vertices())
            {
                std::ostringstream error_msg;
                error_msg << "Not a triangle! Read " << face_total_vertices << " vertices, expected "
                          << Face::total_vertices()
                          << " (at line " << total_vertices + face_idx + 2 << ")";
                throw std::out_of_range(error_msg.str());
            }

            // reading face's vertices
            FOR_RELATIVE_INDEX(i)
            {
                off_file >> face.vertices[i];
            }

            // checking for adjacent faces
            FOR_RELATIVE_INDEX(i)
            {
                auto edge = std::make_pair(face.vertex(i), face.vertex_mod(i + 1));
                auto edge_inverse = std::make_pair(edge.second, edge.first);
                if (faces_and_opposite_vertex.count(edge_inverse) > 0)
                {
                    const auto &value = faces_and_opposite_vertex[edge_inverse];
                    uint idx = (i + 2) % Face::total_vertices();
                    // adding adjacent face since one was found in the map
                    face.adjacent_faces[idx] = value.first;
                    mesh.face(value.first).adjacent_faces[value.second] = face_idx;
                }
                else
                {
                    // no adjacent face present in the map
                    faces_and_opposite_vertex[edge] = {face_idx, (i + 2) % Face::total_vertices()};
                    // setting the current face as incident face for the corresponding vertex
                    mesh.vertex(edge.first).face = face_idx;
                    mesh.vertex(edge.second).face = face_idx;
                }
            }
        }
    }

    if (mesh.is2D())
    {
        mesh.calculate_circumcenters();
    }

    DEBUG_APPEND("Done");
}

void MeshLoader::loadTetrahedron(Mesh &mesh)
{
    DEBUG("Loading tetrahedron");
    mesh.clear();
    mesh._vertices.reserve(4);
    mesh._vertices.emplace_back(Point(-1. / std::sqrt(3.), 0, -.5), 0);
    mesh._vertices.emplace_back(Point(1. / std::sqrt(3.), 0, -.5), 1);
    mesh._vertices.emplace_back(Point(0, 0, .5), 2);
    mesh._vertices.emplace_back(Point(0, 1., 0), 3);

    mesh._faces.reserve(4);
    mesh._faces.emplace_back(0, 2, 1, 1, 3, 2); // easy from drawn net
    mesh._faces.emplace_back(1, 2, 3, 2, 3, 0);
    mesh._faces.emplace_back(0, 3, 2, 1, 0, 3);
    mesh._faces.emplace_back(0, 1, 3, 1, 2, 0);
}

void MeshLoader::loadSquarePyramid(Mesh &mesh)
{
    DEBUG("Loading square pyramid");
    mesh.clear();
    mesh._vertices.reserve(5);
    mesh._vertices.emplace_back(Point(-.5, 0, -.5), 0);
    mesh._vertices.emplace_back(Point(-.5, 0, .5), 1);
    mesh._vertices.emplace_back(Point(.5, 0, .5), 2);
    mesh._vertices.emplace_back(Point(.5, 0, -.5), 3);
    mesh._vertices.emplace_back(Point(.0, 1., .0), 4);

    mesh._faces.reserve(6);
    mesh._faces.emplace_back(0, 1, 3, 1, 4, 5);
    mesh._faces.emplace_back(1, 2, 3, 3, 0, 2);
    mesh._faces.emplace_back(1, 4, 2, 3, 1, 5);
    mesh._faces.emplace_back(4, 3, 2, 1, 2, 4);
    mesh._faces.emplace_back(3, 4, 0, 5, 0, 3);
    mesh._faces.emplace_back(4, 1, 0, 0, 4, 2);
}

void MeshLoader::loadRandomPlane(Mesh &mesh, uint width, uint height, uint slices, bool random)
{
    // AWFUL CODE DO NOT READ AM SORRY
    DEBUG("Loading random plane");
    mesh.clear();

    const uint slices_width = std::max(width / slices / 2, 1u);
    const uint slices_height = std::max(height / slices / 2, 1u);
    mesh._vertices.reserve(std::min(slices, width) * std::min(slices, height));
    uint rows{0}, cols{0};
    for (uint y = slices_height; y < height; y += 2 * slices_height)
    {
        cols = 0;
        for (uint x = slices_width; x < width; x += 2 * slices_width)
        {
            constexpr double div = 1.7;
            double rand_x = x;
            double rand_y = y;
            if (random)
            {
                // keeping it convex
                if (x != slices_width && x + 2 * slices_width < width)
                { // not border
                    rand_x = Random::range(x - uint(slices_width / div),
                                           std::min(x + uint(slices_width / div), width - 1u));;
                }
                if (y != slices_height && y + 2 * slices_height < height)
                {
                    rand_y = Random::range(y - uint(slices_height / div),
                                           std::min(y + uint(slices_height / div), height - 1u));
                }
            }
            mesh._vertices.emplace_back(Point(rand_x - width / 2, rand_y - height / 2, 0) * 1e-2);
            cols++;
        }
        rows++;
    }
    mesh._vertices.emplace_back(infinite);
    // I tried to use numeric_limits<double>::infinity(), alas it is treated as zero when displaying

    uint vertex_inf = mesh._vertices.size() - 1;
    assert(rows == (height - slices_height) / (2 * slices_height) + 1);
    assert(cols == (width - slices_width) / (2 * slices_width) + 1);


    const auto index = [&cols](uint x, uint y)
    { return x + y * cols; };
    const auto face_index = [&cols](uint x, uint y)
    { return 2 * y * (cols - 1) + 2 * x; };

    for (uint y = 0; y < rows - 1; ++y)
    {
        for (uint x = 0; x < cols - 1; ++x)
        {
            uint face_idx = face_index(x, y);

            /*      ⋮
             * 10_____11____
             * |\  1  |\  3
             * |  \   |  \    ⋯
             * | 0  \ | 2  \
             * 0-----1------3-
             *
             * Example with 10×10 grid: starting with vertex 0 on the first row (bottom),
             * vertex 10 just the row above. The cell they form are split in two (thus the two
             * emplace_back). face_idx(i,j) returns the corresponding face to a vertex, e.g.
             * face_idx(0,0)=0, face_idx(1,0)=2 etc. (up right face)
             */
            mesh._faces.emplace_back(index(x, y), index(x + 1, y), index(x, y + 1), face_idx + 1, face_idx - 1,
                                     face_index(x, y - 1) + 1);
            FOR_RELATIVE_INDEX(i)
            {
                mesh.vertex(mesh.face(mesh._faces.size() - 1).vertex(i)).face = mesh._faces.size() - 1;
            }
            mesh._faces.emplace_back(index(x + 1, y), index(x + 1, y + 1), index(x, y + 1), face_index(x, y + 1),
                                     face_idx, face_idx + 2);
            FOR_RELATIVE_INDEX(i)
            {
                mesh.vertex(mesh.face(mesh._faces.size() - 1).vertex(i)).face = mesh._faces.size() - 1;
            }
        }
    }

    mesh._vertices[mesh._vertices.size() - 1].face = mesh._faces.size();
    // Infinite faces (faces linked to inf. vertex) ==> faces outside the convex hull
    // bottom row
    for (uint x = 0; x < cols - 1; ++x)
    {
        index_t lf = x == 0 ? mesh._faces.size() + 2 * (rows + cols - 2) - 1 : mesh._faces.size() - 1;
        index_t face_idx = 2 * x;
        mesh._faces.emplace_back(index(x, 0), vertex_inf, index(x + 1, 0), mesh._faces.size() + 1, face_idx, lf);
        mesh.face(face_idx).set_adjacent_face(2, mesh._faces.size() - 1);
    }

    // right column
    for (uint y = 0; y < rows - 1; ++y)
    {
        index_t face_idx = face_index(cols - 2, y) + 1;
        mesh._faces.emplace_back(index(cols - 1, y), vertex_inf, index(cols - 1, y + 1), mesh._faces.size() + 1,
                                 face_idx, mesh._faces.size() - 1);
        mesh.face(face_idx).set_adjacent_face(2, mesh._faces.size() - 1);
    }

    // top row
    for (uint x = cols - 1; x > 0; --x)
    {
        index_t face_idx = face_index(x - 1, rows - 2) + 1;
        mesh._faces.emplace_back(index(x, rows - 1), vertex_inf, index(x - 1, rows - 1), mesh._faces.size() + 1,
                                 face_idx, mesh._faces.size() - 1);
        mesh.face(face_idx).set_adjacent_face(0, mesh._faces.size() - 1);
    }

    // left column
    for (uint y = rows - 1; y > 0; --y)
    {
        index_t lf = y == 1 ? 2 * ((rows - 1) * (cols - 1)) : mesh._faces.size() + 1;
        index_t face_idx = face_index(0, y - 1);
        mesh._faces.emplace_back(index(0, y), vertex_inf, index(0, y - 1), lf, face_idx, mesh._faces.size() - 1);
        mesh.face(face_idx).set_adjacent_face(1, mesh._faces.size() - 1);
    }

    mesh._type = Mesh::Type::MESH2D;
    mesh.calculate_circumcenters();
}
