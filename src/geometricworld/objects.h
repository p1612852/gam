#ifndef OBJECTS_H
#define OBJECTS_H

#include <array>
#include "mat.h"

namespace gmw
{
    using index_t = unsigned long long;
    using relative_index_t = unsigned char;

    struct Vertex
    {
        Point position;
        index_t face; /// adjacent face

        Vertex() = default;

        explicit Vertex(const Point &position_) : position(position_), face()
        {}

        Vertex(const Point &position_, index_t face_) : position(position_), face(face_)
        {}
    };

    using Edge = std::pair<index_t, index_t>;

    struct Face
    {
    private:
        static constexpr relative_index_t TOTAL_VERT = 3;

    public:
        std::array<index_t, TOTAL_VERT> vertices;
        std::array<index_t, TOTAL_VERT> adjacent_faces;

        Face() = default; // to allow default initialization in containers

        Face(index_t v0, index_t v1, index_t v2,
             index_t f0, index_t f1, index_t f2)
                : vertices{v0, v1, v2}, adjacent_faces{f0, f1, f2}
        {}

        constexpr static relative_index_t total_vertices()
        { return Face::TOTAL_VERT; }

        /**
         * Returns the corresponding vertex, looping around the array if the
         * given index is greater than its size.
         */
        index_t vertex_mod(relative_index_t idx) const;

        index_t vertex(relative_index_t idx) const;

        /**
         * Returns the corresponding adjacent face, looping around the array if the
         * given index is greater than its size.
         */
        index_t adjacent_face_mod(relative_index_t idx) const;

        index_t adjacent_face(relative_index_t idx) const;

        void set_vertex_mod(relative_index_t idx, index_t vertex_idx);

        void set_adjacent_face_mod(relative_index_t idx, index_t face_idx);

        void set_adjacent_face(relative_index_t idx, index_t face_idx);

        /// @return The relative index of the vertex's face
        relative_index_t relative(index_t vertex) const;

        bool is_adjacent_face(index_t face_idx) const;
    };

#define FOR_RELATIVE_INDEX(I) for (relative_index_t I = 0; I < Face::total_vertices(); ++I)
}

#endif //OBJECTS_H
