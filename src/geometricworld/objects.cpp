#include <cassert>
#include <misc/misc.h>
#include <cmath>
#include "objects.h"

using namespace gmw;

index_t Face::vertex_mod(relative_index_t idx) const
{
    return vertices[idx % Face::total_vertices()];
}

index_t Face::vertex(relative_index_t idx) const
{
    return vertices[idx];
}

index_t Face::adjacent_face_mod(relative_index_t idx) const
{
    return adjacent_faces[idx % Face::total_vertices()];
}

index_t Face::adjacent_face(relative_index_t idx) const
{
    return adjacent_faces[idx];
}

void Face::set_vertex_mod(relative_index_t idx, index_t vertex_idx)
{
    vertices[idx % Face::total_vertices()] = vertex_idx;
}

void Face::set_adjacent_face_mod(relative_index_t idx, index_t face_idx)
{
    adjacent_faces[idx % Face::total_vertices()] = face_idx;
}

void Face::set_adjacent_face(relative_index_t idx, index_t face_idx)
{
    adjacent_faces[idx] = face_idx;
}

relative_index_t Face::relative(index_t vertex) const
{
    FOR_RELATIVE_INDEX(i)
    {
        if (vertices[i] == vertex)
        {
            return i;
        }
    }

    throw std::logic_error("This face doesn't contain any vertex of id " + std::to_string(vertex));
}

bool Face::is_adjacent_face(index_t face_idx) const
{
    FOR_RELATIVE_INDEX(i)
    {
        if (adjacent_faces[i] == face_idx)
        {
            return true;
        }
    }
    return false;
}