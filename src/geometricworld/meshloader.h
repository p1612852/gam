#ifndef MESHLOADER_H
#define MESHLOADER_H

#include "mesh.h"

namespace gmw
{
    /// A simple helper class to load predefined meshes or load them from disk
    class MeshLoader
    {
    public:
        static void loadFromOFF(Mesh &mesh, const char *filename);

        static void loadTetrahedron(Mesh &mesh);

        static void loadSquarePyramid(Mesh &mesh);

        static void
        loadRandomPlane(Mesh &mesh, uint width = 200, uint height = 300, uint slices = 8, bool random = true);
    };
}

#endif //MESHLOADER_H
