#ifndef GEOMETRICWORLD_H
#define GEOMETRICWORLD_H

#include <vector>

#include "mesh.h"

namespace gmw
{
    class GeometricWorld //Generally used to create a singleton instance
    {
    public :
        GeometricWorld();

        void draw(bool is_wireframe, bool display_axes, float scale, bool voronoi) const;

        void loadMesh(size_t mesh_idx, const std::string &mesh_filename);

        const std::vector<std::string> &getMeshNames() const;

        Mesh &getMesh();

        const Mesh &getMesh() const;

        void generatePlane(uint width, uint height, uint slices, bool random);

    private:
        std::vector<Mesh> _mesh;
        size_t _current_mesh_idx;
        const std::vector<std::string> _hardcoded_mesh_names;

        void drawAxes() const;
    };
}
#endif //GEOMETRICWORLD_H
