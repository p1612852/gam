#ifndef GLDISPLAYWIDGET_H
#define GLDISPLAYWIDGET_H

#include <QGLWidget> // Module QtOpengL (classes QGLxxx in Qt4),
// with widget and rendering classes descending from QGLWidget,
// deprecated for module QtGui (classes QOpenGLxxx )
// and module widgets (QWidget, QGraphicsView).
#include <QtWidgets>
#include <QTimer>
#include "geometricworld.h"  // Model

class GLDisplayWidget : public QGLWidget
{
public:
    explicit GLDisplayWidget(QWidget *parent = nullptr);

    void
    initializeGL() override; // The scene may be initialized in this function since the GeometricWorld is a data member...
    void paintGL() override; // Display the scene Gl
    void resizeGL(int width, int height) override;

    void voronoi(bool voronoi);

    void isWireframe(bool is_wireframe);

    void displayAxes(bool display_axes);

    void selectMesh(int mesh_idx, const std::string &mesh_filename);

    void coloring(float coloring);

    void scale(float scale);

    const std::vector<std::string> &getMeshNames() const;

    void insertMode(bool insert_mode);

    void generatePlane(uint width, uint height, uint slices, bool random);

    void lawson();

    void crust();

    bool isMesh2D() const;

protected:
    // Mouse Management
    void mousePressEvent(QMouseEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;

    void wheelEvent(QWheelEvent *event) override;

private:
    QTimer _timer; // To update the scene
    QVector3D _translation;
    QVector2D _angle; // Rotation
    QPoint _last_mouse_pos; // To keep the last position of the mouse

    gmw::GeometricWorld _geom_world; // The scene to be displayed

    bool _voronoi, _is_wireframe, _display_axes, _insert_mode;
    float _scale, _coloring;
    int _mesh_idx;
    std::string _mesh_filename;

};

#endif // GLDISPLAYWIDGET_H
