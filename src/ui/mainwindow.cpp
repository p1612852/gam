#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), _last_directory("/")
{
    ui->setupUi(this);
    for (const std::string &m : ui->widget->getMeshNames())
    {
        ui->comboBox_Mesh->addItem(QString(m.c_str()));
        // TODO populate the dropdown : reading from ./data
    }
    ui->comboBox_Mesh->addItem(_open_file_text);

    ui->pushButton_Generate_Plane->click();
    ui->horizontalSlider_Coloring->valueChanged(ui->horizontalSlider_Coloring->value());
    ui->horizontalSlider_Scale->valueChanged(ui->horizontalSlider_Scale->value());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_checkBox_Wireframe_clicked(bool checked)
{
    ui->widget->isWireframe(checked);
}

void MainWindow::on_checkBox_Axes_clicked(bool checked)
{
    ui->widget->displayAxes(checked);
}

void MainWindow::on_comboBox_Mesh_activated(int index)
{
    meshSelection(index);
}

void MainWindow::on_comboBox_Mesh_currentIndexChanged(int index)
{
    meshSelection(index);
}

void MainWindow::meshSelection(int index)
{
    std::string filename = ui->comboBox_Mesh->currentData().toString().toStdString();
    if (ui->comboBox_Mesh->currentText() == _open_file_text)
    {
        QString full_filename = QFileDialog::getOpenFileName(this, tr("Open File"), _last_directory);
        _last_directory = QFileInfo(full_filename).path();
        filename = QFileInfo(full_filename).filePath().toStdString();

        if (QFile::exists(full_filename))
        {
            bool oldState = ui->comboBox_Mesh->blockSignals(true); // To prevent calling combobox_indexchanged
            ui->comboBox_Mesh->insertItem(index, QFileInfo(full_filename).baseName(), full_filename);
            ui->comboBox_Mesh->blockSignals(oldState);
        }
        else
        {
            index = _last_mesh_idx;
        }
        ui->comboBox_Mesh->setCurrentIndex(index);
    }

    ui->widget->selectMesh(index, filename);
    check_2D();
    _last_mesh_idx = index;
}

void MainWindow::on_horizontalSlider_Coloring_valueChanged(int value)
{
    ui->widget->coloring(1.f * value / ui->horizontalSlider_Coloring->maximum());
}

void MainWindow::on_horizontalSlider_Scale_valueChanged(int value)
{
    constexpr float k = 4.f;
    //TODO
    ui->widget->scale(2.f * value / ui->horizontalSlider_Scale->maximum() / k);
}

void MainWindow::on_pushButton_Insert_clicked(bool checked)
{
    ui->widget->insertMode(checked);
    ui->pushButton_Insert->setChecked(checked);
}

void MainWindow::on_checkBox_Voronoi_clicked(bool checked)
{
    ui->widget->voronoi(checked);
}

void MainWindow::on_pushButton_Generate_Plane_clicked()
{
    int width = ui->spinBox_Width_Plane->value();
    int height = ui->spinBox_Height_Plane->value();
    int slices = ui->spinBox_Slices_Plane->value();
    bool random = ui->checkBox_Random_Plane->isChecked();
    ui->widget->generatePlane(width, height, slices, random);
}

void MainWindow::on_pushButton_Lawson_clicked()
{
    ui->widget->lawson();
}

void MainWindow::on_pushButton_Crust_clicked()
{
    ui->widget->crust();
    check_2D();
}

void MainWindow::check_2D()
{
    ui->groupBox_2D->setEnabled(ui->widget->isMesh2D());
    ui->widget_Scale->setDisabled(ui->widget->isMesh2D());
}
