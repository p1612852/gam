#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow() override;

private slots:

    void on_checkBox_Wireframe_clicked(bool checked);

    void on_checkBox_Axes_clicked(bool checked);

    void on_comboBox_Mesh_activated(int index);

    void on_comboBox_Mesh_currentIndexChanged(int index);

    void on_horizontalSlider_Coloring_valueChanged(int value);

    void on_horizontalSlider_Scale_valueChanged(int value);

    void on_pushButton_Insert_clicked(bool checked);

    void on_checkBox_Voronoi_clicked(bool checked);

    void on_pushButton_Generate_Plane_clicked();

    void on_pushButton_Lawson_clicked();

    void on_pushButton_Crust_clicked();

private:
    Ui::MainWindow *ui;
    QString _last_directory;
    int _last_mesh_idx;
    static constexpr const char *_open_file_text = "<open file>";

    void meshSelection(int index);

    void check_2D();
};

#endif // MAINWINDOW_H
