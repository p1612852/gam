#include "gldisplaywidget.h"

#ifdef __APPLE__
#include <glu.h>
#else

#include <GL/glu.h>

#endif

#include "misc/misc.h"

#include "QDebug"

GLDisplayWidget::GLDisplayWidget(QWidget *parent) : QGLWidget(parent), _translation(0.f, 0.f, 0.f), _angle(0.f, 0.f),
                                                    _voronoi(false), _is_wireframe(false), _display_axes(false),
                                                    _insert_mode(false),
                                                    _scale(1.f), _coloring(0.f), _mesh_idx(0)
{
    // Update the scene
    connect(&_timer, SIGNAL(timeout()), this, SLOT(updateGL()));
    _timer.start(16); // Starts or restarts the timer with a timeout interval of 16 milliseconds.
}

void GLDisplayWidget::initializeGL()
{
    // background color
    glClearColor(0.2, 0.2, 0.2, 1);

    // Shader
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
}

void GLDisplayWidget::paintGL()
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Center the camera
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0); //gluLookAt(eye, center, up)  //deprecated
    // Returns a 4x4 matrix that transforms world coordinates to eye coordinates.
    // Translation
    glTranslated(_translation.x(), _translation.y(), _translation.z());

    // Rotation
    glRotatef(_angle.x(), 0.0f, 1.0f, 0.0f);
    glRotatef(_angle.y(), 1.0f, 0.0f, 0.0f);

    // Color for your _geomWorld
    glColor3f(0, 1, 0);

    _geom_world.draw(_is_wireframe, _display_axes, _scale, _voronoi);
}

void GLDisplayWidget::resizeGL(int width, int height)
{

    glViewport(0, 0, width, height); //Viewport in the world window
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, static_cast<GLfloat>(width) / height, 0.1f, 100.0f);

    updateGL();
}

void GLDisplayWidget::voronoi(bool voronoi)
{
    DEBUG_CONDITION(voronoi, "Voronoi Mesh", "Standard Mesh");
    _voronoi = voronoi;
}

void GLDisplayWidget::isWireframe(bool is_wireframe)
{
    DEBUG_CONDITION(is_wireframe, "Displaying wireframe", "Displaying plainfaces");
    _is_wireframe = is_wireframe;
}

void GLDisplayWidget::displayAxes(bool display_axes)
{
    DEBUG_CONDITION(display_axes, "Displaying axes", "Hiding axes");
    _display_axes = display_axes;
}

const std::vector<std::string> &GLDisplayWidget::getMeshNames() const
{
    return _geom_world.getMeshNames();
}

// - - - - - - - - - - - - Mouse Management  - - - - - - - - - - - - - - - -
// When you click, the position of your mouse is saved
void GLDisplayWidget::mousePressEvent(QMouseEvent *event)
{
    if (event != nullptr)
    {
        _last_mouse_pos = event->pos();
        if (_insert_mode)
        {
            Point point(_last_mouse_pos.x(), _last_mouse_pos.y(), 0);
            point.x = (point.x / this->width() * 2.f - 1.f) * 1.9f;
            point.y = -(point.y / this->height() * 2.f - 1.f) * 2.1f;
            _geom_world.getMesh().insert(point);
            _geom_world.getMesh().calculate_circumcenters();
        }
    }
}

// Mouse movement management
void GLDisplayWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (_insert_mode)
        return;

    float dx = event->x() - _last_mouse_pos.x();
    float dy = event->y() - _last_mouse_pos.y();


    float x = _angle.x() + dx;
    float y = _angle.y() + dy;
    _angle.setX(clamp(x, -89.999f, 89.999f)); // I don't want to play with quaternions
    _angle.setY(clamp(y, -89.999f, 89.999f));
    _last_mouse_pos = event->pos();
    updateGL();
}

// Mouse Management for the zoom
void GLDisplayWidget::wheelEvent(QWheelEvent *event)
{
    if (_insert_mode)
        return;

    QPoint numDegrees = event->angleDelta();
    float stepZoom = 0.25f;
    if (!numDegrees.isNull())
    {
        float z = _translation.z();
        z = (numDegrees.x() > 0 || numDegrees.y() > 0) ? z + stepZoom : z - stepZoom;
        _translation.setZ(z);
    }
}

void GLDisplayWidget::selectMesh(int mesh_idx, const std::string &mesh_filename)
{
    _mesh_idx = mesh_idx;
    _mesh_filename = mesh_filename;
    _geom_world.loadMesh(_mesh_idx, _mesh_filename);
}

void GLDisplayWidget::coloring(float coloring)
{
    _coloring = coloring;
}

void GLDisplayWidget::scale(float scale)
{
    DEBUG("Scaling mesh to " << scale);
    _scale = scale;
}

void GLDisplayWidget::insertMode(bool insert_mode)
{
    DEBUG("Insert mode: " << insert_mode);
    _insert_mode = insert_mode;
    if (insert_mode)
    {
        _angle.setX(0);
        _angle.setY(0);
        _translation.setZ(0);
        _scale = 1;
    }
}

void GLDisplayWidget::generatePlane(uint width, uint height, uint slices, bool random)
{
    DEBUG("Generating random plane");
    _geom_world.generatePlane(width, height, slices, random);
    _geom_world.getMesh().calculate_circumcenters();
}

void GLDisplayWidget::lawson()
{
    DEBUG("Applying Lawson's algorithm");
    _geom_world.getMesh().lawson();
    _geom_world.getMesh().calculate_circumcenters();
}

void GLDisplayWidget::crust()
{
    DEBUG("Applying Crust's algorithm");
    _geom_world.getMesh().calculate_circumcenters();
    _geom_world.getMesh().crust();
}

bool GLDisplayWidget::isMesh2D() const
{
    return _geom_world.getMesh().is2D();
}
