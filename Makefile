THREADS=4
BUILD_TYPE=Debug
BUILD_LOC=build
BUILD_TYPE_LC=$(shell echo $(BUILD_TYPE) | tr A-Z a-z)

all:
	mkdir -p $(BUILD_LOC)
	cd $(BUILD_LOC) && cmake .. -DCMAKE_BUILD_TYPE=$(BUILD_TYPE) && cmake --build . --target Mesh_Computational_Geometry -j $(THREADS)
	@printf "\nExecute the compiled program via ./$(BUILD_LOC)/Mesh_Computational_Geometry\n"

qmake:
	mkdir -p $(BUILD_LOC)
	cd $(BUILD_LOC) && qmake .. CONFIG+=$(BUILD_TYPE_LC) && make -j$(THREADS)
	@printf "\nExecute the compiled program via ./$(BUILD_LOC)/Mesh_Computational_Geometry\n"

clean:
	rm -Ir $(BUILD_LOC)

TARNAME=FRATI_Claude.tar.gz
tar:
	tar zcvf $(TARNAME) Makefile README.md report/* examples/* src/* CMakeLists.txt Mesh_Computational_Geometry.pro
