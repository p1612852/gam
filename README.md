# *Mesh and Computational Geometry* Lab

Project available on [UCBL's Gitlab](https://forge.univ-lyon1.fr/p1612852/gam).

## Author

- Claude FRATI

## Build & execution

Requires C++11, Qt (tested with Qt 5.15) and OpenGL.

### Build with CMake

Requires CMake >3.10. With the help of a Makefile wrapper:
```shell script
make [OPTIONS]

Options:
THREADS=<int> BUILD_TYPE={Debug,Release}
PRINT_DEBUG={1,0} BUILD_LOC=<location>
```

### Build with Qt 

Open the `.pro` project with Qt and build it, or try
directly with `qmake`. You can also make use of the Makefile
wrapper:

```shell script
make qmake [OPTIONS]
```

You need to clear the build directory in order to change
the build type after executing the previous command.

### Execution

When executing the program with the argument `test`, it outputs
the valency of every vertex contained in `data/cube.off`.
In other cases, the program launches the GUI.

## Lab work

- [Lab 1](report/lab1.md)
- [Lab 2](report/lab2.md) (26/10/20)
- [Lab 3](report/lab3.md) (04/01/21)