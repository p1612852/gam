# Lab 2

## Code structure

### Files

All operations performed on a 2D mesh
are located inside the [Mesh][] class, section 'Delaunay' (l. 61-96).
The file [mat.h][] contains elementary geometric predicates.

The [MeshLoader][] class was enhanced with a random plane generator, which
was quite hard to write to keep the data structure coherent.

### Operations

#### Edge flipping, triangle splitting

Edge flipping and triangle splitting are both located inside the [Mesh][]
class. Nothing special about these two functions.

#### Inserting a point

The insertion one gave me way more headaches. The walking
algorithm turned out to be simple, and always works as
long as the mesh is convex -- which is exactly our case.

What was really hard was when inserting outside the convex hull:
I have to make use of the `Circulator` iterators we had to develop previously.
Doing so I realised I made some mistakes (I also later realised that I 'needed'
a const version of them, making the templated class a little more cumbersome to write).

The resulting code by circulating left and right on the sides is quite ugly and
a little hard to understand without any drawings or understanding of how the data
structure is implemented (which is pretty obvious).

### Geometric predicates

The basic tests were easy to write. But the calculation of the circumcenter
was harder to comprehend. I tried to implement the method we've seen in class,
without any success. I know I have to test for edge cases on the `tan` operation:
when the angle is $`\frac{\pi}{4}`$, the circumcenter is the middle of the hypotenuse.
So I fell back on the method shown on [Wikipedia](https://en.wikipedia.org/wiki/Barycentric_coordinate_system#Examples_of_special_points),
using the following as barycentric coordinates: $`H(sin(2\hat{A}),sin(2\hat{B}),sin(2\hat{C}))`$ (and normalize).

### Lawson algorithm

The lawson algorithm is also fairly simple. 
The function signature is `lawnson(index_t * vertex_idx = nullptr)`.
It takes an optional argument. If not defined, the function will
apply the lawson algorithm on the entire mesh. Otherwise, it applies
an incremental version of it, starting at the given vertex.
Everytime an edge is flipped, I insert in a queue up to two edges
(defined as pairs of face indexes) that need to be tested.  

But it still has some limitations on edge cases, 
notably with right triangles.
Which is expected because of the floating point approximations.  

### Voronoi

Displaying the Voronoi pattern is really easy because of its
duality with Delaunay. Every time the mesh changes, I recalculate
all of the faces circumcenters. It is not efficient at all,
I'd better be off with each Face storing an additional point.
But I'd still have to recalculate for each face, as I don't have
a way of knowing which face has potentially changed. Or could I?
Now thinking about it I probably could, because I do know which
faces changed during any operation.


## Examples

See [`examples/meshgeom-delaunay.webm`](../examples/meshgeom-delaunay.webm) for a demo.

![](../examples/plane.png)
![](../examples/plane-voronoi.png)
![](../examples/plane-voronoi-wireframe.png)



 


[Mesh]: ../src/geometricworld/mesh.h
[mat.h]: ../src/misc/mat.h
[MeshLoader]: ../src/geometricworld/meshloader.h