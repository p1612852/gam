# Lab 3

## Flashback: Lawson algorithm

Turns out: my Lawson algorithm implementation does not work.
The incremental version of it does seem to work but probably
by chance (the dataset I have is small).

## Power Crust

The Power Crust algorithm I implemented does not work either.
It does produces a recognizable silhouette, but I have a 
hard linking the input vertices correctly, probably due to
either a bad mesh loading or a incorrect lawson algorithm,
and the resulting mesh is not Delaunay.

## Simplification

I also tried a while ago to do a naive simplification
by collapsing (*contracting*?) edges. There would have
been a simplification percentage. Alas, I chose to do the
Power Crust algorithm...

## Examples

![](../examples/bunny-crust.png)



 


[Mesh]: ../src/geometricworld/mesh.h
[mat.h]: ../src/misc/mat.h
[MeshLoader]: ../src/geometricworld/meshloader.h