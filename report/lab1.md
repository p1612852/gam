# Lab 1

## Code structure

### Files

The original `mesh.h` header is now split into 4 different files:

- [mesh.h][] contains the `Mesh` class
holding the data structure we've seen in class
- [meshloader.h][] is a helper
to load various meshes (predefined and from OFF files)
- [objects.h][] for geometric objects used by `Mesh`,
namely `Point`, `Vertex` and `Face`
- [meshiterators.h][] contains completed iterators on
`Mesh`'s vertices and faces (including circulators 
and their `const` version).

### Loading a mesh from an OFF file

The corresponding function `loadFromOFF(...)` is defined
in [meshloader.cpp][].
It is identical as what we studied in class, it makes use
of a map to hold data about adjacent faces.

### Iterators

I make use of templates, this way I avoid code duplication. I encountered
some issues with template partial specializations that I managed to resolve:
I wanted specialized
methods for both `Face` and `Vertex` when incrementing 
and dereferencing the `Circulator` iterators.

## Examples

See [`examples/meshgeom.webm`](../examples/meshgeom.webm) for a demo.

![](../examples/cube.jpg)
![](../examples/queen-plain.jpg)
![](../examples/queen-wireframe.jpg)

[mesh.h]: ../src/geometricworld/mesh.h
[meshloader.h]: ../src/geometricworld/meshloader.h
[meshloader.cpp]: ../src/geometricworld/meshloader.cpp
[objects.h]: ../src/geometricworld/objects.h
[meshiterators.h]: ../src/geometricworld/meshiterators.h